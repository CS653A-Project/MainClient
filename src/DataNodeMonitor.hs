{-# LANGUAGE OverloadedStrings, DeriveGeneric #-}

import Network.Socket
import Data.Heap
import Data.Time.Clock
import Data.ByteString.Lazy (ByteString)
import Data.ByteString.Lazy.Internal
import Data.Aeson 
import Control.Applicative
import Control.Monad
import GHC.Generics
import System.Environment

--------------------------------------------- Globals ---------------------------------------------------

serverPort = 5555

--------------------------------------------- Data Types ------------------------------------------------

data QueryType = Update | Get deriving (Show, Generic)   -- Query Data Type for Client Query Type

instance FromJSON QueryType

data DataNodeQuery = DataNodeQuery {            -- JSON Data Type for Client Query
    queryType :: QueryType,
    ipAddress :: String,
    diskSpace :: Integer 
} deriving (Show, Generic)

instance FromJSON DataNodeQuery                 -- For parsing incoming query from String to DataNodeQuery

data DataNode = DataNode {                      -- Data Type for storing into maxHeap
    ipAddress' :: String,
    diskSpace' :: Integer,
    lastUpdated :: IO UTCTime
}

instance Show DataNode where                    -- Explicitly defined Show instance of DataNode because IO UTCTime is not an instance of Show
    show (DataNode ip space _) = 
        "IP Address : " ++ ip ++ ", Disk Space : " ++ show space

type DataNodeMaxHeap = MaxPrioHeap Integer DataNode

--------------------------------------------- Functions --------------------------------------------------

-- Helper Function to filter data nodes from maxHeap

ipNotEqual :: String -> (Integer, DataNode) -> Bool
ipNotEqual ip (_,dataNode) = ipAddress' dataNode /= ip

-- Function to handle queries of different types (Update or Get)

handleQuery :: Maybe DataNodeQuery -> Socket -> IO DataNodeMaxHeap -> IO DataNodeMaxHeap
handleQuery Nothing clientSocket oldHeap = do                                   -- For Corrupt Query
    send clientSocket "Unable to parse your query"
    oldHeap

handleQuery (Just (DataNodeQuery Update ip space)) clientSocket oldHeap = do    -- For Update Query 
    oldHeap' <- oldHeap
    let newHeap = Data.Heap.filter (ipNotEqual ip) oldHeap'
    send clientSocket "Updated"
    currentTime <- getCurrentTime
    return $ insert (space, DataNode ip space $ return currentTime) newHeap

handleQuery (Just (DataNodeQuery Get ip space)) clientSocket oldHeap = do       -- For Get Query
    oldHeap' <- oldHeap
    case view oldHeap' of
        Nothing -> do
            send clientSocket "No Data Node Available"
            return Data.Heap.empty
        Just ((sp, DataNode ip spCopy updationTime),restHeap) -> do
            currentTime <- getCurrentTime
            updationTime' <- updationTime
            if diffUTCTime currentTime updationTime' < 10  then do
                --case view restHeap of
                --    Nothing -> do
                --        send clientSocket ip
                --        oldHeap
                --    Just((_, DataNode ip2 _ updationTime2), restHeap2) -> do
                --        updationTime2' <- updationTime2
                --        if (diffUTCTime currentTime updationTime2' < 10) then do
                --            send clientSocket (ip ++ "," ++ ip2)
                --            oldHeap
                --        else do
                --            send clientSocket ip
                --            return $ insert (sp, DataNode ip sp updationTime) restHeap2
                send clientSocket ip
                oldHeap  
            else
                handleQuery (Just (DataNodeQuery Get ip space)) clientSocket $ return restHeap 

-- Function to handle connected clients

handleClient :: (Socket, SockAddr) -> IO DataNodeMaxHeap -> IO DataNodeMaxHeap
handleClient (clientSocket, ip) oldHeap = do
    receivedMsg <- recv clientSocket 256
    let query = decode $ packChars receivedMsg :: Maybe DataNodeQuery
    putStrLn $ "Query - " ++ show query ++ " from " ++ show ip 
    newHeap <- handleQuery query clientSocket oldHeap
    close clientSocket
    return newHeap

-- Function to connect to clients

connectToClient :: Socket -> IO DataNodeMaxHeap -> IO DataNodeMaxHeap
connectToClient serverSocket oldHeap = do
    connectedClient <- accept serverSocket                          -- Accept one connection and handle it
    let (h,host) = connectedClient
    putStrLn $ "Connected to Client : " ++ show host
    newHeap <- handleClient connectedClient oldHeap
    putStrLn $ "Updated Heap - " ++ show newHeap ++ "\n"
    connectToClient serverSocket $ return newHeap

-- Main Function

main :: IO()
main = do
    [ipAddr] <- getArgs
    newServerSocket <- socket AF_INET Stream 0                      -- Create Socket
    setSocketOption newServerSocket ReuseAddr 1                     -- Make socket immediately reusable - eases debugging.
    host <- inet_addr ipAddr
    bindSocket newServerSocket (SockAddrInet serverPort host)     -- Listen on specified TCP port
    putStrLn $ "Server Started at " ++ ipAddr ++ ":" ++ show serverPort
    listen newServerSocket 10                                       -- Allow a maximum of 10 outstanding connections
    connectToClient newServerSocket $ return Data.Heap.empty
    putStrLn ""