{-# LANGUAGE OverloadedStrings #-}

import Data.List.Split
import Control.Monad
import FileUploader (runFileUploader, encryptAndUpload, UploadResponse(..))
import FileRetrieval (retrieveAndDecrypt)

import System.Path.NameManip (guess_dotdot, absolute_path)
import System.FilePath (addTrailingPathSeparator, normalise)
import System.Directory (getHomeDirectory, doesFileExist)
import Data.Maybe (fromJust, isJust)
import Data.List (isPrefixOf)

import System.Environment
import Database.HDBC as DH
import Database.HDBC.Sqlite3 (connectSqlite3, Connection)

import System.IO
import Control.Exception
import Data.Digest.Pure.SHA as DDPS
import Data.ByteString.Lazy.Internal as DBLI
import System.FilePath.Posix

import System.Exit
import Data.ByteString.Internal as DBI

initDataBase :: IO Connection 
initDataBase = connectSqlite3 "main_client.db"

initTables :: Connection -> String -> IO Connection
initTables dbConn x = if x == "init" then 
    do
        run dbConn "DROP TABLE IF EXISTS filesystem" []
        run dbConn "DROP TABLE IF EXISTS auth" []
        run dbConn "CREATE TABLE filesystem (owner VARCHAR(255) NOT NULL, realName VARCHAR(255) NOT NULL, initChunk VARCHAR(255) NOT NULL, ipList VARCHAR(70) NOT NULL)" []
        run dbConn "CREATE TABLE auth (username VARCHAR(255) NOT NULL, password VARCHAR(70) NOT NULL)" []
        commit dbConn
        return dbConn
    else return dbConn

absolutize :: String -> IO String
absolutize aPath 
    | "~" `isPrefixOf` aPath = do
        homePath <- getHomeDirectory
        return $ normalise $ addTrailingPathSeparator homePath 
                             ++ tail aPath
    | otherwise = do
        pathMaybeWithDots <- absolute_path aPath
        return $ fromJust $ guess_dotdot pathMaybeWithDots

executeCommand :: Connection -> String -> String -> Command -> String -> IO (Maybe String)
executeCommand dbConn user _ Ls _ = printLs dbConn user
executeCommand _ _ _ Exit _ = exitSuccess
executeCommand _ _ _  Invalid _ = return $ Just "Error: Invalid command or usage."
executeCommand dbConn user pass (Upload filepath remoteName) serverIp = do
    absPath <- absolutize filepath
    res <- doesFileExist absPath
    if res
        then do
        response <- encryptAndUpload absPath pass serverIp
        run dbConn "INSERT into filesystem VALUES (?,?,?,?)" [toSql user, toSql remoteName, toSql (getInitChunk response), toSql (show $ getIpList response)]
        commit dbConn
        return Nothing
        else 
            return $ Just "Local file does not exists."
executeCommand dbConn user pass (Download i_n o_ut) _ = do 
    res <- quickQuery' dbConn "SELECT * from filesystem where owner=? and realName=?" [toSql user, toSql i_n]
    if null res then
        return $ Just "File does not exists."
    else do 
        retrieveAndDecrypt o_ut ((\[[_,_, SqlByteString x, _]] -> DBI.unpackChars x) res) ((\[[_,_, _, SqlByteString x]] -> (read $ DBI.unpackChars x) :: [String]) res) pass
        return Nothing

printLs :: Connection -> String -> IO (Maybe String)
printLs dbConn user = do
    res <- quickQuery' dbConn "SELECT * from filesystem where owner=?" [toSql user]
    print res
    return Nothing

data Command = 
    Ls
    | Invalid
    | Download String String
    | Upload String String
    | Exit

formatInput :: [String] -> Command
formatInput [x, y] = case x of
                        "upload" -> Upload y (takeFileName y)
                        _ -> Invalid
formatInput [x, y, z] = case x of
                        "upload" -> Upload y z
                        "download"-> Download y z
                        _ -> Invalid
formatInput [x] = case x of 
                        "ls" -> Ls
                        "exit" -> Exit
                        _ -> Invalid

runInterpreter :: Connection -> String -> String -> String -> IO ()
runInterpreter dbConn user password serverIp = do 
    getCommands dbConn user password serverIp
    runInterpreter dbConn user password serverIp

getCommands :: Connection -> String -> String -> String -> IO ()
getCommands dbConn user pass serverIp = do
    putStr "$ "
    input <- getLine
    let splitInput = splitOn " " input
    response <- executeCommand dbConn user pass (formatInput splitInput) serverIp
    when (isJust response) $ putStrLn ((\(Just x) -> x) response)
    return ()

getPassword :: IO String
getPassword = do
  putStr "Password: "
  hFlush stdout
  pass <- withEcho False getLine
  putChar '\n'
  return pass

withEcho :: Bool -> IO a -> IO a
withEcho echo action = do
  old <- hGetEcho stdin
  bracket_ (hSetEcho stdin echo) (hSetEcho stdin old) action

initUser :: Connection -> IO [String]
initUser dbConn = do
    putStr "New Username: "
    username <- getLine
    res <- quickQuery' dbConn "SELECT * from auth where username=?" [toSql username]
    if not (null res) then do
        putStrLn "Error: User already exists."
        exitFailure
    else do 
        pass <- getPassword
        run dbConn "INSERT INTO auth (username, password) VALUES (?,?)" [toSql username, toSql (show $ DDPS.sha256 $ DBLI.packChars pass)]
        commit dbConn
        return [username, pass]

doLogin :: Connection -> IO [String]
doLogin dbConn = do
    putStr "Username: "
    username <- getLine
    res <- quickQuery' dbConn "SELECT * from auth where username=?" [toSql username]
    if null res then do
        putStrLn "Error: No such user exists."
        exitFailure
    else do 
        pass <- getPassword
        res <- quickQuery' dbConn "SELECT * from auth where username=? and password=?" [toSql username, toSql (show $ DDPS.sha256 $ DBLI.packChars pass)]
        if length res /= 1 then do 
            putStrLn "Error: Authentication Failure."
            exitFailure
        else return [username, pass]


main :: IO ()
main = do 
    dbConn <- initDataBase
    [x, y, z] <- getArgs
    if x == "init" then 
        if y == "register" then do
            initTables dbConn "init"
            [user, pass] <- initUser dbConn
            runInterpreter dbConn user pass z
        else do 
            putStrLn "Error: Invalid usage."
            exitFailure
    else
        if x == "restore" then
            if y == "register" then  do
                [user, pass] <- initUser dbConn
                runInterpreter dbConn user pass z
            else
                if y == "login" then do 
                    [user, pass] <- doLogin dbConn
                    runInterpreter dbConn user pass z
                else do
                    putStrLn "Error: Invalid usage."
                    exitFailure
        else do 
            putStrLn "Error: Invalid usage."
            exitFailure
    return ()







