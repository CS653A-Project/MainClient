{-# LANGUAGE OverloadedStrings, DeriveGeneric #-}

module FileUploader (runFileUploader, encryptAndUpload, UploadResponse(..)) where

import Network.Socket
import GHC.Generics
import Data.ByteString.Lazy (ByteString)
import Data.ByteString.Lazy.Internal
import Data.Aeson 
import Network.FTP.Client
import Control.Concurrent
import System.IO
import Foreign.Marshal.Array
import Foreign.Marshal.Alloc
import Foreign.Ptr
import Data.Time
import Data.List as DL
import System.Directory as SD
import System.Environment
import System.Process as SP
import Data.List.Split (splitOn)

serverPort = 5555

getQuery = DataNodeQuery Get "" (-1)

--------------------------------------------- Data Types ------------------------------------------------

data QueryType = Update | Get deriving (Show, Generic)   -- Query Data Type for Client Query Type

instance ToJSON QueryType

data DataNodeQuery = DataNodeQuery {            -- JSON Data Type for Client Query
    queryType :: QueryType,
    ipAddress :: String,
    diskSpace :: Integer 
} deriving (Show, Generic)

instance ToJSON DataNodeQuery                   -- For parsing incoming query from String to DataNodeQuery

data NextPartQuery = NextPartQuery {
    getCurrChunk :: String,
    getNextChunk :: String,
    getIp :: String,
    getQueryType :: String
} deriving (Show, Generic)

instance ToJSON NextPartQuery where
    toJSON (NextPartQuery getCurrChunk getNextChunk getIp getQueryType) = 
        object [
            "currChunk" .= getCurrChunk,
            "nextChunk" .= getNextChunk,
            "ip" .= getIp,
            "queryType" .= getQueryType
            ]

--------------------------------------------- Functions --------------------------------------------------

-- Function to transfer file through FTP 

transferFile :: String -> [String] -> IO ()
transferFile filename ipList =                   -- This filename is without path
    case ipList of
        [] -> return ()
        (ip:ips) -> do
            --enableFTPDebugging 
            ftpConnection <- easyConnectFTP ip
            putStrLn $ "\nConnected to FTP Server : " ++ ip
            login ftpConnection "botnet" (Just "botnet") Nothing 
            uploadbinary ftpConnection filename
            putStrLn $ filename ++ " file transferred"
            quit ftpConnection
            putStrLn "FTP Connection Closed"
            transferFile filename ips

-- Function to send query to the server and get its reply

getIPsFromServer :: Socket -> IO [String]
getIPsFromServer serverSocket = do
    let msg = unpackChars $ encode getQuery
    send serverSocket msg
    putStrLn $ "Query Sent - " ++ show getQuery
    receivedMsg <- recv serverSocket 256
    putStrLn $ "Server's reply - " ++ receivedMsg
    --return $ splitOn "," receivedMsg
    return [receivedMsg]

connectNewSocket addressInfo = do
    let serverAddress = head addressInfo
    newSocket <- socket (addrFamily serverAddress) Stream defaultProtocol
    connect newSocket (addrAddress serverAddress)
    return newSocket

-- Function to connect to server after every 3 seconds

distributeOnServers :: String -> String -> Int -> IO [String]
distributeOnServers filename ipAddress port = do
    addressInfo <- getAddrInfo Nothing (Just ipAddress) (Just $ show port)
    newSocket <- connectNewSocket addressInfo
    putStrLn $ "\nConnected To Server - " ++ ipAddress ++ ":" ++ show port
    ipList <- getIPsFromServer newSocket
    sClose newSocket
    putStrLn "Connection Closed"
    transferFile filename ipList
    --transferFile filename ["127.0.0.1"]
    return ipList

tellToPreviousIP :: String -> String -> String -> String -> IO ()
tellToPreviousIP prevFileName filename prevIP currIP = do
    let port = 12346         
    addressInfo <- getAddrInfo Nothing (Just prevIP) (Just $ show port)
    newSocket <- connectNewSocket addressInfo
    putStrLn $ "\nConnected To Botnet Server - " ++ prevIP ++ ":" ++ show port
    let nextPartQuery = NextPartQuery {getCurrChunk = prevFileName, getNextChunk = filename, getIp = currIP, getQueryType = "insert"}
    
    let msg = unpackChars $ encode nextPartQuery
    send newSocket msg
    putStrLn $ "Query Sent - " ++ show nextPartQuery
    receivedMsg <- recv newSocket 256
    putStrLn $ "Server's reply - " ++ receivedMsg

    sClose newSocket
    putStrLn "Connection Closed"

getFileName :: IO String
getFileName = do
    res <- getCurrentTime
    return $  concat (splitOn " " (concat (splitOn ":" $ show res)))

tellToPreviousIPss :: String -> String -> String -> [String] -> IO ()
tellToPreviousIPss _ _ _ [] = return ()
tellToPreviousIPss prevName filePartName x (y:ys) = do
    tellToPreviousIP prevName filePartName x y
    tellToPreviousIPss prevName filePartName x ys

recurseTell :: String -> String -> [String] -> [String] -> IO ()
recurseTell _ _ [] _ = return ()
recurseTell _ _ _ [] = return ()
recurseTell prevName filePartName (x:xs) ys = do
    tellToPreviousIPss prevName filePartName x ys
    recurseTell prevName filePartName xs ys

data UploadResponse = UploadResponse {
    getIpList :: [String],
    getInitChunk :: String
} | None

divideFile :: Handle -> String -> Ptr Int -> Int -> IO [String] -> String -> IO UploadResponse
divideFile inputFile prevFilePartName buffer bufferSize prevPartIPs  serverIP = do
    bytesRead <- hGetBuf inputFile buffer bufferSize
    if bytesRead > 0 then do
        filePartName <- getFileName
        filePart <- openBinaryFile filePartName WriteMode
        hPutBuf filePart buffer bytesRead
        hClose filePart
        currentPartIPs <- distributeOnServers filePartName serverIP serverPort
        prevPartIPs' <- prevPartIPs 
        print currentPartIPs
        print prevPartIPs'
        recurseTell prevFilePartName filePartName prevPartIPs' currentPartIPs
        --tellToPreviousIP prevFilePartName filePartName (head currentPartIPs) (head prevPartIPs')
        -- let k = [tellToPreviousIP prevFilePartName filePartName prev curr | prev <- prevPartIPs', curr <- currentPartIPs] 
        SD.removeFile filePartName
        divideFile inputFile filePartName buffer bufferSize (return currentPartIPs) serverIP
        return UploadResponse {getIpList = currentPartIPs, getInitChunk = filePartName}
    else
        --prevPartIPs' <- prevPartIPs
        --let k = [tellToPreviousIP inputFileName prev curr | prev <- prevPartIPs', curr <- ["EOF"]] 
        return None

-- Main Function

main :: IO ()
main = do
    [filepath, ip] <- getArgs
    runFileUploader filepath ip
    return ()

runFileUploader :: String -> String -> IO UploadResponse
runFileUploader absInputFilePath serverIP = do
    let bufferSize = 1048576
    buffer <- mallocArray bufferSize  :: IO (Ptr Int)
    inputFile <- openBinaryFile absInputFilePath ReadMode 
    divideFile inputFile "" buffer bufferSize (return []) serverIP

encryptAndUpload :: String -> String -> String -> IO UploadResponse
encryptAndUpload path pass serverIP = do
    SP.callCommand ("aescrypt -e -p " ++ pass ++ " -o temp_encrypted " ++ path)
    runFileUploader "temp_encrypted" serverIP