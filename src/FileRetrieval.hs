{-# LANGUAGE OverloadedStrings, DeriveGeneric #-}

module FileRetrieval (retrieveAndDecrypt) where

import Network.Socket
import GHC.Generics
import Data.ByteString.Lazy (ByteString)
import Data.ByteString.Lazy.Internal
import Data.Aeson 
import Network.FTP.Client
import Control.Concurrent
import System.IO
import Foreign.Marshal.Array
import Foreign.Marshal.Alloc
import Foreign.Ptr
import Control.Applicative
import Control.Monad
import Data.Maybe
import System.Process as SP
import Control.Exception

data Message = Message {
    getIp :: Maybe String,
    getCurrChunk :: String,
    getNextChunk :: Maybe String,
    getQueryType :: String
} deriving (Show, Generic)

instance ToJSON Message where
    toJSON (Message getIp getCurrChunk getNextChunk getQueryType) = object [
                                                "currChunk" .= getCurrChunk,
                                                "queryType" .= getQueryType,
                                                "ip" .= getIp,
                                                "nextChunk" .= getNextChunk
                                                ]

data QueryResult = QueryResult {
    getValue :: Maybe String,
    getIpList :: [String]
} deriving (Show, Generic)

instance FromJSON QueryResult where
    parseJSON (Object v) = QueryResult <$>
                            v .:? "value" <*>
                            v .: "ipList"
    parseJSON _ = mzero

downloadFile :: String -> [String] -> IO Bool
downloadFile filename ipList =                  -- This filename is without path
    case ipList of
        [] -> return False
        (ip:ips) -> do
            --enableFTPDebugging 
            ftpConnection <- easyConnectFTP ip
            putStrLn $ "\nConnected to FTP Server : " ++ ip
            login ftpConnection "botnet" (Just "botnet") Nothing 
            downloadbinary ftpConnection filename
            putStrLn $ filename ++ " file downloaded"
            quit ftpConnection
            putStrLn "FTP Connection Closed"
            return True
            --downloadFile filename ips)

getNextChunkIPs :: Message -> String -> Int -> IO (Maybe QueryResult)
getNextChunkIPs queryMsg ipAddress port = do
    addressInfo <- getAddrInfo Nothing (Just ipAddress) (Just $ show port)
    let serverAddress = head addressInfo
    newSocket <- socket (addrFamily serverAddress) Stream defaultProtocol
    connect newSocket (addrAddress serverAddress)
    putStrLn $ "\nConnected To Server - " ++ ipAddress ++ ":" ++ show port
    
    let msg = unpackChars $ encode queryMsg
    send newSocket msg
    putStrLn $ "Query Sent - " ++ show queryMsg
    receivedMsg <- recv newSocket 256
    let queryResult = decode $ packChars receivedMsg :: Maybe QueryResult
    putStrLn $ "Server's query reply - " ++ show queryResult

    sClose newSocket
    putStrLn "Connection Closed"
    return queryResult

retrieveFile outputFile partFileName currServerIPs buffer bufferSize = do
    x <- downloadFile partFileName currServerIPs
    if x then do
        inputPartFile <- openBinaryFile partFileName ReadMode
        bytesRead <- hGetBuf inputPartFile buffer bufferSize
        hPutBuf outputFile buffer bytesRead
        let queryMsg = Message Nothing partFileName Nothing "retrieve"
        let ip = head currServerIPs
        queryResult <- getNextChunkIPs queryMsg ip 12346
        case queryResult of
            Nothing -> do
                putStrLn "Unable to parse server reply"
                return ()
            Just q -> case getValue q of 
                Nothing -> do
                    hClose outputFile
                    return ()
                Just nextPartFileName -> 
                    if null (getIpList q) then do
                        hClose outputFile
                        putStrLn "Empty list of IPs returned by the server"
                        return ()
                    else retrieveFile outputFile nextPartFileName (getIpList q) buffer bufferSize
    else do
        putStrLn "Unable to download file"
        hClose outputFile
        return ()

main :: IO ()
main = do
    let outputFileName = "newFile"
    let partFileName = "partFile"
    let currServerIPs = ["127.0.0.1"]
    runRetrieval outputFileName partFileName currServerIPs

runRetrieval :: String -> String -> [String] -> IO ()
runRetrieval outputFileName partFileName currServerIPs = do
    let bufferSize = 1048576
    buffer <- mallocArray bufferSize  :: IO (Ptr Int)
    outputFile <- openBinaryFile outputFileName WriteMode
    retrieveFile outputFile partFileName currServerIPs buffer bufferSize 

retrieveAndDecrypt :: String -> String -> [String] -> String -> IO ()
retrieveAndDecrypt outputFile partFileName currServerIPs pass = do
    runRetrieval "temp_decrypt" partFileName currServerIPs
    SP.callCommand ("aescrypt -d -p " ++ pass ++ " -o " ++ outputFile ++ " temp_decrypt")
